import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Warehouse {
    private final List<WarehouseSection> locations;

    public Warehouse() {
        this.locations = new ArrayList<>();
    }

    public List<WarehouseSection> getLocations() {
        return locations;
    }

    public void addLocation(WarehouseSection location) {
        this.locations.add(location);
        System.out.println("Added location " + location.getName() + " to the warehouse");
    }

    public Product findProduct(int productId) {
        for (WarehouseSection location : locations) {
            for (Product product : location.getProducts()) {
                if (product.getId() == productId) {
                    System.out.println("Product " + product.getName() + " found at location " + location.getName());
                    return product;
                }
            }
        }
        System.out.println("Product with ID " + productId + " not found in the warehouse");
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Warehouse warehouse = (Warehouse) o;
        return Objects.equals(locations, warehouse.locations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(locations);
    }

    @Override
    public String toString() {
        return "Warehouse{" +
                "locations=" + locations +
                '}';
    }
}
