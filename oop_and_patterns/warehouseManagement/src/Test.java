public class Test {
    public static void main(String[] args) {
        Warehouse warehouse = new Warehouse();

        WarehouseSection locationA = new WarehouseSection(1, "A", 10);
        WarehouseSection locationB = new WarehouseSection(2, "B", 5);

        warehouse.addLocation(locationA);
        warehouse.addLocation(locationB);

        Product product1 = new Product(101, "Widget", 10, 50);
        Product product2 = new Product(102, "Gadget", 20, 30);

        locationA.addProduct(product1);
        locationB.addProduct(product2);
        locationB.removeProduct(product2.getId());

        warehouse.findProduct(102);
        warehouse.findProduct(101);
    }
}
