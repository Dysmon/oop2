import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class WarehouseSection {
    private final int id;
    private final String name;
    private final int capacity;
    private final List<Product> products;

    WarehouseSection(int id, String name, int capacity) {
        this.id = id;
        this.name = name;
        this.capacity = capacity;
        this.products = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getCapacity() {
        return capacity;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void addProduct(Product product) {
        if (this.products.size() < this.capacity) {
            this.products.add(product);
            System.out.println("Added " + product.getName() + " to location " + this.name);
        } else {
            System.out.println("Location " + this.name + " is full.");
        }
    }

    public void removeProduct(int productId) {
        int index = -1;
        for (int i = 0; i < this.products.size(); i++) {
            if (this.products.get(i).getId() == productId) {
                index = i;
                break;
            }
        }

        if (index != -1) {
            Product removedProduct = this.products.remove(index);
            System.out.println("Removed " + removedProduct.getName() + " from location " + this.name);
        } else {
            System.out.println("Product with ID " + productId + " not found in location " + this.name);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WarehouseSection location = (WarehouseSection) o;
        return id == location.id && capacity == location.capacity && Objects.equals(name, location.name) && Objects.equals(products, location.products);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, capacity, products);
    }

    @Override
    public String toString() {
        return "Location{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", capacity=" + capacity +
                ", products=" + products +
                '}';
    }
}

