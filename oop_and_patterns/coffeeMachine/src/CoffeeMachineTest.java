public class CoffeeMachineTest {
    public static void main(String[] args) {
        Beverage cappuccino = new Cappuccino();
        cappuccino = new Milk(cappuccino);
        cappuccino = new Cream(cappuccino);
        cappuccino = new Chocolate(cappuccino);

        Beverage darkRoast = new DarkRoast();
        darkRoast = new Milk(new Whip(new Whip(darkRoast)));

        System.out.printf("Beverage and condiments are: %s, the price is %s\n", cappuccino.getDescription(), cappuccino.cost());
        System.out.printf("Beverage and condiments are: %s, the price is %s", darkRoast.getDescription(), darkRoast.cost());
    }
}
