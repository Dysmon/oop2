public class Cream extends CondimentDecorator {
    private final Beverage beverage;

    public Cream(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public double cost() {
        return beverage.cost() + .4;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ", cream";
    }
}
