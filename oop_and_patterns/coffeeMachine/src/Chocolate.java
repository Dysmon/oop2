public class Chocolate extends CondimentDecorator {
    private final Beverage beverage;
    public Chocolate(Beverage beverage) {
        this.beverage = beverage;
    }
    @Override
    public double cost() {
        return beverage.cost() + .9;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ", chocolate";
    }
}
