public class Rook implements Piece {
    public boolean isLegalMove(Position a, Position b) {
        return a.getColumn() == b.getColumn() || a.getRow() == b.getRow();
    }
}
