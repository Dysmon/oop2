public interface Piece {
    abstract boolean isLegalMove(Position a, Position b);
}
