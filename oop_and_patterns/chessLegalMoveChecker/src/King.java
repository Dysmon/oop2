public class King implements Piece {
    public boolean isLegalMove(Position a, Position b) {
        return Math.abs(a.getRow() - b.getRow()) <= 1 && Math.abs(a.getColumn() - b.getColumn()) <= 1;
    }
}
