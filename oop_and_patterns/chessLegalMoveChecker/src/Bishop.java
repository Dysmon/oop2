public class Bishop implements Piece {
    public boolean isLegalMove(Position a, Position b) {
        return Math.abs(a.getColumn() - b.getColumn()) == Math.abs(a.getRow() - b.getRow());
    }
}
