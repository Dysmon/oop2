import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        Position a1 = new Position('a', '1');
        Position a2 = new Position('a', '7');
        Position b3 = new Position('b', '3');
        Position h8 = new Position('h', '8');

        Piece queen = new Queen();
        Piece king = new King();
        Piece rook = new Rook();
        Piece bishop = new Bishop();
        Piece knight = new Knight();

        List<Piece> pieces = List.of(queen, king, rook, bishop, knight);

        for (Piece k : pieces) {
            System.out.println(k.isLegalMove(a1, h8));
        }
    }
}
