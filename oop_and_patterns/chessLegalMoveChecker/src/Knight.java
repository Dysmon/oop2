import static java.lang.Math.abs;

public class Knight implements Piece {
    public boolean isLegalMove(Position a, Position b) {
        if (abs(a.getColumn() - b.getColumn()) == 1 && abs(a.getRow() - b.getRow()) == 2 ||
                abs(a.getColumn() - b.getColumn()) == 2 && abs(a.getRow() - b.getRow()) == 1) {
            return true;
        }
        return false;
    }
}
