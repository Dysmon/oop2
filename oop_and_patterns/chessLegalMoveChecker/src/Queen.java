public class Queen implements Piece {
    public boolean isLegalMove(Position a, Position b) {
        return new Bishop().isLegalMove(a, b) ||
                new Knight().isLegalMove(a, b);
    }
}
