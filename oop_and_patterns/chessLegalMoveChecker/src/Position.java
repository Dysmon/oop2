public class Position {
    private char column;
    private char row;

    Position(char column, char row) {
        this.column = column;
        this.row = row;
    }

    public char getColumn() {
        return column;
    }

    public char getRow() {
        return row;
    }

    public String toString(){
        return column + "" + row;
    }
}